class NotificationService {
    constructor() {
        this.io = null;
    }

    setIO(io) {
        io.on('connection', socket => {
            console.log('Connected');
            this.io = io;
        });
    }

    send(notificationMessage) {
        if (this.io === null) {
            return;
        }
        this.io.sockets.emit('notification', notificationMessage);
    }
}

export const notificationService = new NotificationService();
