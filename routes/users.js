let express = require('express');
let User = require('../models/users.js');
let Admin = require('../models/admin.js');
let Order = require('../models/order');
let Product = require('../models/product')
let jwt = require('jsonwebtoken');
let router = express.Router();
let app = express();


router.post('/admin', (req, res) => {
    Admin.login(req.body, (err, user) => {
        if (err || !user || !user.length) {
            res.status(401).json({
                title: 'Unable to log in',
                error: {message: 'Bad credentials'}
            });
        }
        else {
            const payload = {
                username: user.username
            };
            let token = jwt.sign(payload, req.app.get('privateKey'), {
                expiresIn: 144000
            });
            res.json({
                success: true,
                token: token
            });
        }
    });
});


router.post('/user', (req, res) => {
    User.login(req.body, (err, user) => {
        if (err || !user || !user.length) {
            res.status(401).json({
                title: 'Unable to log in',
                error: {message: 'Bad credentials'}
            });
        }
        else {
            const payload = {
                username: user.username
            };
            let token = jwt.sign(payload, req.app.get('privateKey'), {
                expiresIn: 144000
            });
            res.json({
                success: true,
                token: token
            });
        }
    });
});


router.post('/new', (req, res) => {
    let user = new User(req.body);
    User.addUser(user, (err, user) => {
        if (err) {
            throw err;
        }
        res.json(user);
    })
});

router.put('/orders/:username', (req, res) => {
    User.addOrder(req.params.username, req.body._id, (err, user) => {
        if (err) {
            throw err;
        }
        res.json(user);
    })
});

router.post('/cartProduct/:username', (req, res) => {
    User.addProductToCart(req.params.username, req.body._id, (err, user) => {
        if (err) {
            throw err;
        }
        res.json(user);
    })
});

router.delete('/cartProduct/:username/:productId', (req, res) => {
    User.removeProductFromCart(req.params.username, req.params.productId, (err, user) => {
        if (err) {
            throw err;
        }
        res.json(user);
    })
});

router.delete('/cartProduct/:username/', (req, res) => {
    User.removeProductFromCartAll(req.params.username, req.params.productId, (err, user) => {
        if (err) {
            throw err;
        }
        res.json(user);
    })
});

router.get('/cartProduct/:username', (req, res) => {
    User.get(req.params.username, (err, user) => {
        Product.findAllProducts(user.cartProductsIds, (err, docs) => {
                res.json(docs);
            })
        })
    });

    router.get('/orders/:username', (req, res) => {
        User.get(req.params.username, (err, user) => {
            Order.findAllOrders(user.ordersIds, (err, docs) => {
                res.json(docs);
            })
        })
    });

    module.exports = router;