let express = require('express');
let Order = require('../models/order');
let Product = require('../models/product');
let Auth = require('../Util/Auth');

let router = express.Router();

router.use((req, res, next) => {
    Auth.authorize(req, res, next);
});

function reduceItemQuantity(productId) {
    Product.reduceQuantity(productId, (error, product) => {
        if (error || !product) {
            return false;
        }
        return true;
    })
}

function reduceAllItemQuantity(orderedProductsIds, callback) {
    let promises = [];
    for (let orderedId of orderedProductsIds) {
        promises.push(reduceItemQuantity(orderedId));
    }
    let areAllAvailable = true;
    Promise.all(promises).then( (returnValue)  => {
        if (returnValue === false) {
            areAllAvailable = false;
        }
    });
}

router.get('/', (req, res) => {
    Order.getOrders((err, orders) => {
        if (err) {
            res.send(err);
        }
        res.json(orders);
    });
});

router.post('/', (req, res) => {
    delete req.body._id;
    let order = new Order(req.body)
    if (true)  {
        Order.saveOrder(order,(error) => {
            if (error) {
                console.log(error);
                res.send({error : error});
            }
            res.send(order);
        });
    }
    else {
        res.send({status : "Items not available"});
    }
});

router.put('/', (req, res) => {
    let order = new Order(req.body)
        Order.markAsCompleted(order,(error, order) => {
            if (error) {
                console.log(error);
                res.send({error : error});
            }
            res.send(order);
        });
});


module.exports = router;
