import {notificationService} from "../NotificationService";

let express = require('express');
var app = express();

let Product = require('../models/product.js');
let Discount = require('../models/Discount');
let Auth = require('../util/Auth')
let router = express.Router();

router.use((req, res, next) => {
    Auth.authorize(req, res, next);
});

router.post('/add', (req, res) => {
    delete req.body._id;
    let product = new Product(req.body);
    Product.addProduct(product, (err, product) => {
        if (err) {
            throw err;
        }
        notificationService.send("Product added");
        res.json(product);
    })
})

router.put('/discount', (req, res) => {
    Discount.addDiscount(req.body, (err, discount) => {
        if (err) {
            throw err;
        }
        res.json(discount);
        notificationService.send("Products discounted");
        let discountTimeMS = req.body.durationInMinutes * 60 * 1000;
        setTimeout(() => {
            notificationService.send("Discount ends");
        }, discountTimeMS);
    });
})

router.put('/update', (req, res) => {
    let product = new Product(req.body);
    Product.updateProduct(product, (err, product) => {
        if (err) {
            throw err;
        }
        notificationService.send("Product updated");
        res.json(product);
    });

});

router.delete('/:productId', (req, res) => {
    console.log(req.params.productId);
    Product.removeProduct(req.params.productId, (err, product) => {
        if (err) {
            res.json(err);
        }
        else
            notificationService.send("Product deleted");
            res.json(product);
        })
});


module.exports = router;