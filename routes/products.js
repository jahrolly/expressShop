import {discountService} from "../DiscountService";
let express = require('express');
let Product = require('../models/product.js');
let Auth = require('../util/Auth')
let router = express.Router();

router.get('/', (req, res) => {
    Product.getAvailableProducts((err, products) => {
        if (err) {
            res.send(err);
        }
        discountService.includeDiscounts((products), (products) => {
            res.send(products);
        });
    });
});

router.get('/all', (req, res) => {
    Product.getAllProducts((err, products) => {
        if (err) {
            res.send(err);
        }
        else {
            res.json(products);
        }
    });
});

module.exports = router;
