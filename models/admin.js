var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var adminSchema = new Schema({
    username : String,
    password : String
}, {collection: 'admins'});

var Admin = module.exports = mongoose.model('admin', adminSchema);

module.exports.login = (user,  callback) => {
    Admin.find({username: user.username, password : user.password}, callback);
}