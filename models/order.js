var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var orderSchema = new Schema({
    name: String,
    surname: String,
    email: String,
    address: String,
    phoneNumber: Number,
    orderedProductsIds: [String],
    isCompleted : {type: Boolean, default: false}
}, {collection: 'order'});

var Order = module.exports = mongoose.model('order', orderSchema);

module.exports.saveOrder = function (order, callback) {
    console.log("module.exports.saveOrder invoked");
    Order.create(order, callback);
}

module.exports.markAsCompleted = function (order, callback) {
    console.log(order);
    Order.findOneAndUpdate({_id : order._id}, {$set: {"isCompleted" : true}}, callback);
}

module.exports.getOrders = (callback) => {
    Order.find(callback);
}

module.exports.findAllOrders = (ordersIds, callback) => {
    Order.find({_id: {$in : ordersIds}}, callback);
}