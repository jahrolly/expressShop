var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    username : String,
    password : String,
    name: String,
    surname: String,
    email: String,
    address: String,
    phoneNumber: Number,
    ordersIds: [Schema.Types.ObjectId],
    cartProductsIds: [Schema.Types.ObjectId]
}, {collection: 'users'});

var User = module.exports = mongoose.model('user', userSchema);

module.exports.get = (usernameString,  callback) => {
    User.findOne({username: usernameString}, callback);
}

module.exports.login = (user,  callback) => {
    User.find({username: user.username, password: user.password}, callback);
}

module.exports.addUser = (user, callback) => {
    user.save(callback);
}

module.exports.addOrder = (username, orderId, callback) => {
    User.findOneAndUpdate({username : username}, {$push : {ordersIds : orderId}}, callback)
}

module.exports.addProductToCart = (username, productId, callback) => {
    User.findOneAndUpdate({username : username}, {$push : {cartProductsIds : productId}}, callback)
}

module.exports.removeProductFromCart = (username, productId, callback) => {
    User.findOneAndUpdate({username : username}, {$pull : {cartProductsIds : productId}}, callback)
}

module.exports.removeProductFromCartAll = (username, productId, callback) => {
    User.findOneAndUpdate({username : username}, {set : {cartProductsIds : []}}, callback)
}