import {notificationService} from "../NotificationService";

let mongoose = require('mongoose');

let Schema = mongoose.Schema;
let productSchema = new Schema({
    name: String,
    price: Number,
    quantity: Number,
    category: String,
    description: String,
    images: [String],
    DiscountedAmount: {type : Number, default : 0}
}, {collection: 'product'});

let Product = module.exports = mongoose.model('product', productSchema);

module.exports.getAvailableProducts = (callback) => {
    Product.find({ quantity: { $gt: 0 }}, callback);
}

module.exports.getAllProducts = (callback) => {
    Product.find(callback);
}

module.exports.reduceQuantity = (productId, callback) => {
    Product.findOneAndUpdate({ _id : productId, quantity : {$gt : 0}}, {"$inc": {"quantity" : -1}}, callback);
}

module.exports.updateProduct = (product, callback) => {
    Product.findOneAndUpdate({_id: product._id}, product, callback);
}

module.exports.addProduct = (product, callback) => {
    product.save(callback);
}

module.exports.removeProduct = (productId, callback) => {
    Product.remove({_id: productId}, callback);
}

module.exports.findAllProducts = (productsIds, callback) => {
    Product.find({_id: {$in : productsIds}}, callback);
}