import {notificationService} from "../NotificationService";

let mongoose = require('mongoose');

let Schema = mongoose.Schema;
let discountSchema = new Schema({
    discountAmount: Number,
    discountStartTime: Date,
    discountEndTime: Date,
    ids: [String],
    discountPercent: Number
}, {collection: 'discount'});

let Discount = module.exports = mongoose.model('discount', discountSchema);

module.exports.addDiscount = (discountJson, callback) => {
    let discount = new Discount();
    discount.discountPercent= discountJson.discountPercent;
    discount.ids = discountJson.ids;
    discount.discountStartTime = Date.now();
    let discountTimeMS = discountJson.durationInMinutes * 60 * 1000;
    discount.discountEndTime = Date.now() + discountTimeMS;
    discount.save(callback);
}

module.exports.getApplicableDiscounts = (callback) => {
    Discount.find({discountEndTime:{"$gte":Date.now()}}, callback)
}