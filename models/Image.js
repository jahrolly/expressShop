const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './uploads')
    },

    filename: (req, file, callback) => {
        callback(null, file.originalname);
    }
});

const upload = multer({storage: storage}).single('file');

exports.addFile = (req, res) => {
    upload(req, res, error => {
        if (error) {
            res.json({status: error});
            return;
        }
        res.json({fileName: req.file});
    });
};