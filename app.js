let express = require('express');
let config = require('./config');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let jwt = require('jsonwebtoken');
const cors = require('cors');
mongoose.Promise = global.Promise;

let app = express();
app.use(cors({
    origin: 'http://localhost:4200',
    credentials: true
}));
const server = require('http' ).createServer(app);
const io = require('socket.io' )(server);
server.listen(config.socketPort, () => console.log(`SocketIo running on ${config.url}:${config.socketPort}`));


import {notificationService} from "./NotificationService";
notificationService.setIO(io);

mongoose.connect(config.database, { useMongoClient: true });
let db = mongoose.connection;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.set('privateKey', config.privateKey); // secret letiable

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

let order = require('./routes/orderAuth');
let products = require('./routes/products');
let productsAuth = require('./routes/productsAuth');
let users = require('./routes/users');
let image = require('./routes/imageAuth');

app.use('/orders', order);
app.use('/products', products);
app.use('/productsAuth', productsAuth);
app.use('/users', users);
app.use('/imageAuth', image);


app.use('/static', express.static(path.join(__dirname, '/uploads')))

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found');
  console.log("Received unknown request")
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
