let Discount = require('./models/Discount')

class DiscountService {
    includeDiscounts(products, callback) {
        Discount.getApplicableDiscounts((err, discounts) => {
            for (let discount of discounts) {
                let discountFraction = 1 - (discount.discountPercent / 100);
                products.filter(product => discount.ids.includes(String(product._id))).forEach(product => {
                    if (product.DiscountedAmount == 0) {
                        product.DiscountedAmount = product.price;
                    }
                    product.DiscountedAmount *= discountFraction;
                })
            }
            callback(products);
        })
    }
}

export const discountService = new DiscountService();
